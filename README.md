# Go Tester

Minimal docker image with latest gotest.tools/gotestsum and github.com/boumenot/gocover-cobertura binaries to make go-test stages faster

This requires installing dependencies, so theres basically no gain and test ./... doesn't work.

Need to rethink the approach on this to avoid downloading deps once to test, then downloading again to build the containerfile.